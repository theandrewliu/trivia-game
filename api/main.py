from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.encoders import jsonable_encoder
import psycopg

app = FastAPI()


@app.get("/")
def root():
    return {"message": "Hello World"}

@app.get("/api/categories")
def list_categories(page: int=0):
    with psycopg.connect("postgresql://trivia-game@db/trivia-game") as conn:
        with conn.cursor() as cur:
            cur.execute("""
                SELECT id, title, canon
                FROM categories
                LIMIT 100 OFFSET %s
            """,[page*100])
            records=cur.fetchall()
            results = []
            for record in records:
                result = {
                    "id":record[0],
                    "title":record[1],
                    "canon":record[2],
                }
                results.append(result)
            return results

@app.get("/api/categories/{category_id}")
def get_category(category_id:int):
    with psycopg.connect("postgresql://trivia-game@db/trivia-game") as conn:
        with conn.cursor() as cur:
            cur.execute("""
                SELECT id, title, canon
                FROM categories
                WHERE id = %s
                """,[category_id])
            record = cur.fetchone()
            return{
                "id":record[0],
                "title":record[1],
                "canon":record[2],
            }

@app.delete("/api/categories/{category_id}")
def delete_category(category_id:int):
    with psycopg.connect("postgresql://trivia-game@db/trivia-game") as conn:
        with conn.cursor() as cur:
            cur.execute("""
                DELETE FROM categories
                WHERE id = %s
                """,[category_id])
            return "Successfully deleted"
            

class Category(BaseModel):
    title: str
    canon: bool

@app.post("/api/categories")
def create_category(category:Category):
    print(category)
    return "Woo!"
    
@app.put("/api/categories/{category_id}")
def edit_category(category_id:int, category_title:str):
    update_category_encoded = jsonable_encoder(category_title)
    return "Successfully updated"
    